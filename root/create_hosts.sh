#!/bin/bash

populer_hosts(){
	ip=$1
	serv=$2
	
	grep -q $ip /etc/hosts
	existe=$?
	echo $existe

	if  ! (grep -q $ip /etc/hosts); then
	    echo $ip $serv >>/etc/hosts
        fi
}

populer_iptables(){
	ip=$1
	serv=$2

}

for site in $(cat $* )
  do
	adr=$(dig $site +short)
	populer_hosts $adr $site
	populer_iptables $adr $site
  done
