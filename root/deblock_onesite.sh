#!/bin/bash

iptables -A OUTPUT_HTTP_WHITELIST --destination $1 -j ACCEPT
iptables -A OUTPUT_HTTPS_WHITELIST --destination $1 -j ACCEPT
